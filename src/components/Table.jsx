import { Component } from "react";
import Swal from "sweetalert2";
class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  changeColorBtnHandler = (e) => {
    // console.log(e)
    this.props.updateStatus(e);
    // console.log(this.props.status);
  };

  alertHandler = (item) => {
    Swal.fire({
      title:
        "ID : " +
        item.id +
        " \nEmail : " +
        item.email +
        " \nUsername : " +
        item.username +
        " \nAge : " +
        item.age,
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
    });
  };

  render() {
    return (
      <div>
        <div className="w-full flex items-center">
          <div className="w-full">
            <table className="w-full text-sm text-left text-gray-500">
              <thead className="text-xs text-center text-gray-700 uppercase bg-yellow-300">
                <tr>
                  <th scope="col" className="px-6 py-3">
                    id
                  </th>
                  <th scope="col" className="px-6 py-3">
                    email
                  </th>
                  <th scope="col" className="px-6 py-3">
                    username
                  </th>
                  <th scope="col" className="px-6 py-3">
                    age
                  </th>
                  <th scope="col" className="px-6 py-3">
                    action
                  </th>
                </tr>
              </thead>
              <tbody>
                {this.props.data.map((item) => (
                  <tr
                    key={item.id}
                    className="bg-white border-b dark:bg-gray-800 text-center even:bg-gray-200"
                  >
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
                    >
                      {item.id}
                    </th>
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
                    >
                      {item.email}
                    </th>
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
                    >
                      {item.username}
                    </th>
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
                    >
                      {item.age}
                    </th>
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap"
                    >
                      <button
                        onClick={() => this.changeColorBtnHandler(item.id)}
                        type="button"
                        style={
                          item.status === "Pending"
                            ? { background: "red", width: "90px" }
                            : { background: "green", width: "90px" }
                        }
                        className="text-white hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2"
                      >
                        {item.status}
                      </button>

                      <button
                        onClick={() => this.alertHandler(item)}
                        type="button"
                        className="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2"
                      >
                        Show more
                      </button>
                    </th>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Table;
