import { Component } from "react";
import Table from "./Table";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [
        // {
        //   id: 1,
        //   email: "kenaa@example.com",
        //   username: "admin",
        //   age: 20,
        //   status: "pending",
        // },
      ],
      emailInput: "",
      usernameInput: "",
      ageInput: "",
    };
  }

  emailInputHandler = (e) => {
    this.setState({
      emailInput: e.target.value,
    });
  };

  usernameInputHandler = (e) => {
    this.setState({
      usernameInput: e.target.value,
    });
  };
  ageInputHandler = (e) => {
    this.setState({
      ageInput: e.target.value,
    });
    // console.log(e);
  };

  registerHandler = () => {
    const newUser = {
      id: this.state.user.length + 1,
      email: this.state.emailInput === "" ? "null" : this.state.emailInput,
      username:
        this.state.usernameInput === "" ? "null" : this.state.usernameInput,
      age: this.state.ageInput === "" ? "null" : this.state.ageInput,
      status: "Pending",
    };

    // const spreadUser = { ...newObj };
    this.setState(
      {
        user: [...this.state.user, newUser],
        emailInput: "",
        usernameInput: "",
        ageInput: "",
      }
      //   () => console.log(this.state.user)
    );
  };

  updateStatusHandler = (e) => {
    // console.log(e);
    let update = this.state.user.map((item) => {
      if (item.id === e) {
        return { ...item, status: item.status === "Done" ? "Pending" : "Done" };
      }
      return item;
    });
    this.setState({ user: update });
  };

  render() {
    return (
      <div>
        <div className="w-full h-full flex flex-col items-center font-mono">
          <div className="w-10/12 mt-10 flex flex-col items-center rounded-lg">
            <div className="w-10/12 flex flex-col items-center ">
              <p className="text-5xl p-10 pb-0 capitalize italic font-bold text-transparent text-center bg-clip-text bg-gradient-to-r from-orange-500 via-brown-500 to-blue-500">
                please fill your information
              </p>

              <div className="p-16 w-full flex flex-col items-center">
                <div className="w-2/3 p-3">
                  <label
                    for="email-address-icon"
                    className="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
                  >
                    Your Email
                  </label>
                  <div className="relative">
                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5 text-gray-500 dark:text-gray-400"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                        <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                      </svg>
                    </div>
                    <input
                      onChange={this.emailInputHandler}
                      type="email"
                      id="email-address-icon"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
                      placeholder="name@gmail.com"
                    />
                  </div>
                </div>

                <div className="w-2/3 p-3">
                  <label
                    for="website-admin"
                    className="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
                  >
                    Username
                  </label>
                  <div className="flex">
                    <span className="inline-flex items-center px-3 text-sm text-gray-900 bg-blue-200 border border-r-0 border-gray-300 rounded-l-md">
                      🥰
                    </span>
                    <input
                      onChange={this.usernameInputHandler}
                      type="text"
                      id="website-admin"
                      className="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5"
                      placeholder="Vann Krya"
                    />
                  </div>
                </div>

                <div className="w-2/3 p-3">
                  <label
                    for="website-admin"
                    className="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
                  >
                    Age
                  </label>
                  <div className="flex">
                    <span className="inline-flex items-center px-3 text-sm text-gray-900 bg-red-200 border border-r-0 border-gray-300 rounded-l-md">
                      ❤️
                    </span>
                    <input
                      onChange={this.ageInputHandler}
                      type="text"
                      id="website-admin"
                      className="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5"
                      placeholder="21"
                    />
                  </div>
                  <div className="w-full p-3 flex flex-col items-center pt-5">
                    <button
                      onClick={this.registerHandler}
                      className="relative w-1/2 inline-flex items-center justify-center p-0.5 mb-10 mr-2 overflow-hidden text-md font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-pink-500 to-orange-400 group-hover:from-pink-500 group-hover:to-orange-400 hover:text-white  focus:ring-4 focus:outline-none focus:ring-pink-200"
                    >
                      <span className="relative w-96 px-5 py-2.5 bg-white  rounded-md group-hover:bg-opacity-0">
                        Register
                      </span>
                    </button>
                  </div>
                  <Table
                    data={this.state.user}
                    updateStatus={this.updateStatusHandler}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Form;
